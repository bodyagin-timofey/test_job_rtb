package net.yoshkin.test_job.config;

import net.yoshkin.test_job.mapper.WidgetMapper;
import net.yoshkin.test_job.pojo.Widget;
import net.yoshkin.test_job.service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;

import java.time.Clock;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

/**
 * Created by TiM on 05.10.2019.
 */
@Configuration
@EnableScheduling
public class AppConfig {

    @Value("${use.database}")
    Boolean useDatabase;

    @Autowired
    WidgetMapper widgetMapper;

    @Autowired
    WidgetCheckFieldsService checkFieldsService;

    @Autowired
    ListParamUtilService listParamUtilService;

    @Bean
    List<Widget> widgetList() {
        return new CopyOnWriteArrayList<>();
    }

    @Bean
    Clock clock() {
        return Clock.systemUTC();
    }

    @Bean
    WidgetService widgetService() {
        if (useDatabase) {
            return new WidgetServiceDbImpl(widgetMapper, listParamUtilService, checkFieldsService);
        } else {
            return new WidgetServiceImpl(widgetList(), clock(), listParamUtilService, checkFieldsService);
        }
    }
}
