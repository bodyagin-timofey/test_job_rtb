package net.yoshkin.test_job.config;

import net.yoshkin.test_job.interceptor.RateLimitInterceptor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

/**
 * Created by TiM on 05.10.2019.
 */

@Configuration
@RefreshScope
public class InterceptorConfiguration implements WebMvcConfigurer {

    @Value("${rate.limit}")
    Integer rateLimit;

    @Bean
    @RefreshScope
    RateLimitInterceptor rateLimitInterceptor() {
        return new RateLimitInterceptor(rateLimit);
    }

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(rateLimitInterceptor());
    }
}
