package net.yoshkin.test_job.controller;

import net.yoshkin.test_job.pojo.ListParam;
import net.yoshkin.test_job.pojo.WidgetFilter;
import net.yoshkin.test_job.pojo.Widget;
import net.yoshkin.test_job.service.WidgetService;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Created by TiM on 05.10.2019.
 */
@RestController
@RequestMapping("/api/widgets")
public class WidgetController {

    private final WidgetService widgetService;

    public WidgetController(WidgetService widgetService) {
        this.widgetService = widgetService;
    }

    @GetMapping("")
    public List<Widget> getAll(
            @RequestParam(required = false, defaultValue = "0") int page,
            @RequestParam(required = false, defaultValue = "0") int size,
            @RequestParam(required = false, defaultValue = "0") int minx,
            @RequestParam(required = false, defaultValue = "0") int maxx,
            @RequestParam(required = false, defaultValue = "0") int miny,
            @RequestParam(required = false, defaultValue = "0") int maxy) {
        ListParam param = new ListParam().setPage(page).setSize(size);
        param.setFilter(new WidgetFilter()
                .setMinX(minx)
                .setMaxX(maxx)
                .setMinY(miny)
                .setMaxY(maxy));
        return widgetService.getAll(param);
    }

    @GetMapping("/{id}")
    public Widget get(@PathVariable String id) {
        return widgetService.get(id);
    }

    @PostMapping("")
    public Widget create(@RequestBody Widget widget) {
        return widgetService.create(widget);
    }

    @PutMapping("")
    public Widget update(@RequestBody Widget widget) {
        return widgetService.update(widget);
    }

    @ResponseStatus(HttpStatus.NO_CONTENT)
    @DeleteMapping("/{id}")
    public void delete(@PathVariable String id) {
        widgetService.delete(id);
    }
}
