package net.yoshkin.test_job.interceptor;

import org.springframework.http.HttpStatus;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.web.servlet.HandlerInterceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.time.Instant;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * Created by TiM on 05.10.2019.
 */
public class RateLimitInterceptor implements HandlerInterceptor {

    public static final String RATE_LIMIT_HEADER = "Rate-Limit";
    public static final String REQUESTS_LEFT_HEADER = "Requests-Left";
    public static final String RESTORE_LIMIT_TIME_HEADER = "Restore-Limit-Time";
    private static final long LIMIT_INTERVAL_MILLIS = 60000;

    AtomicInteger requestsLeft = new AtomicInteger();

    Integer rateLimit;
    Instant restoreLimitTime;

    public RateLimitInterceptor(Integer rateLimit) {
        System.out.println("rateLimit=" + rateLimit);
        this.rateLimit = rateLimit;
        restoreLimit();
    }

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        int reqLeft = requestsLeft.decrementAndGet();
        response.setHeader(RATE_LIMIT_HEADER, rateLimit.toString());
        response.setHeader(REQUESTS_LEFT_HEADER, String.valueOf(reqLeft < 0 ? 0 : reqLeft));
        response.setHeader(RESTORE_LIMIT_TIME_HEADER, restoreLimitTime.toString());
        if (reqLeft < 0) {
            response.setStatus(HttpStatus.TOO_MANY_REQUESTS.value());
            return false;
        }
        return true;
    }

    @Scheduled(fixedRate = LIMIT_INTERVAL_MILLIS)
    private void restoreLimit() {
        restoreLimitTime = Instant.now().plusMillis(LIMIT_INTERVAL_MILLIS);
        requestsLeft.set(rateLimit);
    }
}
