package net.yoshkin.test_job.mapper;

import net.yoshkin.test_job.pojo.ListParam;
import net.yoshkin.test_job.pojo.Widget;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * Created by TiM on 06.10.2019.
 */
@Mapper
public interface WidgetMapper {
    List<Widget> getAll(ListParam param);
    Widget get(String id);
    void create(Widget widget);
    Integer getMaxZIndex();
    void updateAllZIndex(Integer zIndex);
    void update(Widget widget);
    void delete(String id);
}
