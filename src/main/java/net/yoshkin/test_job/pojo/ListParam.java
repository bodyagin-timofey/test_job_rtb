package net.yoshkin.test_job.pojo;

/**
 * Created by TiM on 05.10.2019.
 */
public class ListParam {
    private int page = 0;
    private int size = 0;
    private WidgetFilter filter;

    public int getPage() {
        return page;
    }

    public ListParam setPage(int page) {
        this.page = page;
        return this;
    }

    public int getSize() {
        return size;
    }

    public ListParam setSize(int size) {
        this.size = size;
        return this;
    }

    public WidgetFilter getFilter() {
        return filter;
    }

    public ListParam setFilter(WidgetFilter filter) {
        this.filter = filter;
        return this;
    }
}
