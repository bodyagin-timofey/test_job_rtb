package net.yoshkin.test_job.pojo;

import java.time.Instant;

/**
 * Created by TiM on 04.10.2019.
 */
public class Widget {
    String id;
    int x = 0;
    int y = 0;
    Integer zIndex;
    int width = 0;
    int height = 0;
    Instant lastDate;

    public String getId() {
        return id;
    }

    public Widget setId(String id) {
        this.id = id;
        return this;
    }

    public int getX() {
        return x;
    }

    public Widget setX(int x) {
        this.x = x;
        return this;
    }

    public int getY() {
        return y;
    }

    public Widget setY(int y) {
        this.y = y;
        return this;
    }

    public Integer getzIndex() {
        return zIndex;
    }

    public Widget setzIndex(Integer zIndex) {
        this.zIndex = zIndex;
        return this;
    }

    public int getWidth() {
        return width;
    }

    public Widget setWidth(int width) {
        this.width = width;
        return this;
    }

    public int getHeight() {
        return height;
    }

    public Widget setHeight(int height) {
        this.height = height;
        return this;
    }

    public Instant getLastDate() {
        return lastDate;
    }

    public Widget setLastDate(Instant lastDate) {
        this.lastDate = lastDate;
        return this;
    }
}
