package net.yoshkin.test_job.pojo;

/**
 * Created by TiM on 05.10.2019.
 */
public class WidgetFilter {
    private int minX;
    private int minY;
    private int maxX;
    private int maxY;

    public int getMinX() {
        return minX;
    }

    public WidgetFilter setMinX(int minX) {
        this.minX = minX;
        return this;
    }

    public int getMinY() {
        return minY;
    }

    public WidgetFilter setMinY(int minY) {
        this.minY = minY;
        return this;
    }

    public int getMaxX() {
        return maxX;
    }

    public WidgetFilter setMaxX(int maxX) {
        this.maxX = maxX;
        return this;
    }

    public int getMaxY() {
        return maxY;
    }

    public WidgetFilter setMaxY(int maxY) {
        this.maxY = maxY;
        return this;
    }
}
