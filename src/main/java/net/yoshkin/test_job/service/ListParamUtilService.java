package net.yoshkin.test_job.service;

import net.yoshkin.test_job.pojo.ListParam;

/**
 * Created by TiM on 06.10.2019.
 */
public interface ListParamUtilService {
    void prepareListParam(ListParam param);
}
