package net.yoshkin.test_job.service;

import net.yoshkin.test_job.pojo.ListParam;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

/**
 * Created by TiM on 06.10.2019.
 */
@Service
public class ListParamUtilServiceImpl implements ListParamUtilService {

    private final int defaultPageSize;
    private final int maxPageSize;

    public ListParamUtilServiceImpl(@Value("${page.size.default}") int defaultPageSize,
                                    @Value("${page.size.max}") int maxPageSize) {
        this.defaultPageSize = defaultPageSize;
        this.maxPageSize = maxPageSize;
    }

    @Override
    public void prepareListParam(ListParam param) {
        if (param.getSize() <= 0) {
            param.setSize(defaultPageSize);
        }
        if (param.getSize() > maxPageSize) {
            param.setSize(maxPageSize);
        }
        if (param.getFilter() != null) {
            if (param.getFilter().getMinX() >= param.getFilter().getMaxX()
                    || param.getFilter().getMinY() >= param.getFilter().getMaxY()) {
                param.setFilter(null);
            }
        }
    }
}
