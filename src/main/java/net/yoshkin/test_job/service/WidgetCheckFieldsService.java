package net.yoshkin.test_job.service;

import net.yoshkin.test_job.pojo.Widget;

/**
 * Created by TiM on 06.10.2019.
 */
public interface WidgetCheckFieldsService {

    void check(Widget widget);

}
