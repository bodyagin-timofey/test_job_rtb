package net.yoshkin.test_job.service;

import net.yoshkin.test_job.pojo.Widget;
import org.springframework.stereotype.Service;

/**
 * Created by TiM on 06.10.2019.
 */
@Service
public class WidgetCheckFieldsServiceImpl implements WidgetCheckFieldsService {

    @Override
    public void check(Widget widget) {
        if (widget.getHeight() <= 0) {
            throw new IllegalArgumentException("Height should be greater than zero");
        }
        if (widget.getWidth() <= 0) {
            throw new IllegalArgumentException("Width should be greater than zero");
        }
    }
}
