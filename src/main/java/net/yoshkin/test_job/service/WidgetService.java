package net.yoshkin.test_job.service;

import net.yoshkin.test_job.pojo.ListParam;
import net.yoshkin.test_job.pojo.Widget;

import java.util.List;

/**
 * Created by TiM on 04.10.2019.
 */
public interface WidgetService {
    List<Widget> getAll(ListParam param);
    Widget get(String id);
    Widget create(Widget widget);
    Widget update(Widget widget);
    void delete(String id);
}
