package net.yoshkin.test_job.service;

import net.yoshkin.test_job.mapper.WidgetMapper;
import net.yoshkin.test_job.pojo.ListParam;
import net.yoshkin.test_job.pojo.Widget;
import org.springframework.transaction.annotation.Transactional;

import java.time.Instant;
import java.util.List;
import java.util.UUID;

/**
 * Created by TiM on 06.10.2019.
 */
public class WidgetServiceDbImpl implements WidgetService {

    private final WidgetMapper widgetMapper;
    private final ListParamUtilService listParamUtilService;
    private final WidgetCheckFieldsService checkFieldsService;

    public WidgetServiceDbImpl(WidgetMapper widgetMapper, ListParamUtilService listParamUtilService, WidgetCheckFieldsService checkFieldsService) {
        this.widgetMapper = widgetMapper;
        this.listParamUtilService = listParamUtilService;
        this.checkFieldsService = checkFieldsService;
    }

    @Override
    public List<Widget> getAll(ListParam param) {
        listParamUtilService.prepareListParam(param);
        return widgetMapper.getAll(param);
    }

    @Override
    public Widget get(String id) {
        return widgetMapper.get(id);
    }

    @Transactional
    @Override
    public Widget create(Widget widget) {
        widget.setId(UUID.randomUUID().toString());
        widget.setLastDate(Instant.now());
        checkFieldsService.check(widget);
        updateZIndex(widget);
        widgetMapper.create(widget);
        return widget;
    }

    @Transactional
    @Override
    public Widget update(Widget widget) {
        widget.setLastDate(Instant.now());
        checkFieldsService.check(widget);
        updateZIndex(widget);
        widgetMapper.update(widget);
        return widget;
    }

    @Override
    public void delete(String id) {
        widgetMapper.delete(id);
    }

    private void updateZIndex(Widget widget) {
        if (widget.getzIndex() == null) {
            Integer zIndex = widgetMapper.getMaxZIndex();
            if (zIndex == null) {
                zIndex = 1;
            } else {
                zIndex++;
            }
            widget.setzIndex(zIndex);
        } else {
            widgetMapper.updateAllZIndex(widget.getzIndex());
        }
    }
}
