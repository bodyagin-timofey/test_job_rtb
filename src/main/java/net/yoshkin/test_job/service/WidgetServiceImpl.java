package net.yoshkin.test_job.service;

import net.yoshkin.test_job.pojo.ListParam;
import net.yoshkin.test_job.pojo.WidgetFilter;
import net.yoshkin.test_job.pojo.Widget;
import org.springframework.stereotype.Service;

import java.time.Clock;
import java.util.*;
import java.util.concurrent.locks.ReentrantReadWriteLock;

/**
 * Created by TiM on 04.10.2019.
 */
@Service
public class WidgetServiceImpl implements WidgetService {

    private final List<Widget> widgetList;
    private final Clock clock;
    private final ListParamUtilService listParamUtilService;
    private final WidgetCheckFieldsService checkFieldsService;

    private final HashMap<String, Integer> indexById;
    private final TreeSet<Widget> orderedByX;
    private final ReentrantReadWriteLock locker;

    public WidgetServiceImpl(List<Widget> widgetList,
                             Clock clock,
                             ListParamUtilService listParamUtilService,
                             WidgetCheckFieldsService checkFieldsService) {
        this.widgetList = widgetList;
        this.clock = clock;
        this.listParamUtilService = listParamUtilService;
        this.checkFieldsService = checkFieldsService;

        indexById = new HashMap<>();
        orderedByX = new TreeSet<>(Comparator.comparing(Widget::getX));
        orderedByX.addAll(widgetList);

        locker = new ReentrantReadWriteLock();

        rebuildIndexById();
    }

    @Override
    public List<Widget> getAll(ListParam param) {
        List<Widget> result;
        locker.readLock().lock();
        listParamUtilService.prepareListParam(param);
        try {
            if (param.getFilter() != null) {
                result = filter(param.getFilter());
            } else {
                result = widgetList;
            }

            int start = 0;
            int end = result.size();
            if (param.getPage() > 0) {
                start = (param.getPage() - 1) * param.getSize();
                end = param.getPage() * param.getSize();
            }
            if (start >= result.size()) {
                return new ArrayList<>();
            }
            if (end > result.size()) {
                end = result.size();
            }
            return result.subList(start, end);
        } finally {
            locker.readLock().unlock();
        }
    }

    @Override
    public Widget get(String id) {
        locker.readLock().lock();
        try {
            Integer idx = indexById.get(id);
            if (idx != null) {
                return widgetList.get(idx);
            }
        } finally {
            locker.readLock().unlock();
        }
        return null;
    }

    @Override
    public Widget create(Widget widget) {
        widget.setId(generateId());
        return update(widget);
    }

    @Override
    public Widget update(Widget widget) {
        widget.setLastDate(clock.instant());
        locker.writeLock().lock();
        try {
            checkFieldsService.check(widget);
            if (widget.getId() != null) {
                delete(widget.getId());
            }
            if (widget.getzIndex() == null) {
                if (widgetList.size() > 0) {
                    widget.setzIndex(widgetList.get(widgetList.size() - 1).getzIndex() + 1);
                } else {
                    widget.setzIndex(1);
                }
                widgetList.add(widget);
                orderedByX.add(widget);
            } else {
                int idx = 0;
                for (Widget currWidget: widgetList) {
                    if (currWidget.getzIndex() >= widget.getzIndex()) {
                        break;
                    }
                    idx++;
                }
                widgetList.add(idx, widget);
                orderedByX.add(widget);
                for (int i = idx + 1; i < widgetList.size(); i++) {
                    Widget currWidget = widgetList.get(i);
                    widgetList.get(i).setzIndex(currWidget.getzIndex() + 1);
                }
            }
            rebuildIndexById();
        } finally {
            locker.writeLock().unlock();
        }
        return widget;
    }

    @Override
    public void delete(String id) {
        boolean alreadyLocked = locker.isWriteLockedByCurrentThread();
        if (!alreadyLocked) {
            locker.writeLock().lock();
        }
        try {
            widgetList.removeIf(it -> it.getId().equals(id));
            orderedByX.removeIf(it -> it.getId().equals(id));
            rebuildIndexById();
        } finally {
            if (!alreadyLocked) {
                locker.writeLock().unlock();
            }
        }
    }

    private String generateId() {
        return UUID.randomUUID().toString();
    }

    private List<Widget> filter(WidgetFilter filter) {
        List<Widget> result = new ArrayList<>();
        if (filter.getMinX() >= filter.getMaxX() || filter.getMinY() >= filter.getMaxY()) {
            return result;
        }
        Set<Widget> filteredByX = orderedByX.subSet(new Widget().setX(filter.getMinX()), true, new Widget().setX(filter.getMaxX()), true);
        for (Widget widget: filteredByX) {
            if (widget.getX() + widget.getWidth() <= filter.getMaxX()
                    && widget.getY() >= filter.getMinY()
                    && widget.getY() + widget.getHeight() <= filter.getMaxY()) {
                result.add(widget);
            }
        }
        result.sort(Comparator.comparing(Widget::getzIndex));
        return result;
    }

    private void rebuildIndexById() {
        indexById.clear();
        for (int i = 0; i < widgetList.size(); i++) {
            indexById.put(widgetList.get(i).getId(), i);
        }
    }
}
