package net.yoshkin.test_job.controller;

import net.yoshkin.test_job.pojo.ListParam;
import net.yoshkin.test_job.pojo.Widget;
import net.yoshkin.test_job.service.WidgetService;
import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentCaptor;

import static org.junit.Assert.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

/**
 * Created by TiM on 06.10.2019.
 */
public class WidgetControllerTest {

    private WidgetService widgetService;
    private WidgetController controller;

    @Before
    public void setUp() throws Exception {
        widgetService = mock(WidgetService.class);
        controller = new WidgetController(widgetService);
    }

    @Test
    public void getAll() throws Exception {
        ArgumentCaptor<ListParam> argument = ArgumentCaptor.forClass(ListParam.class);
        controller.getAll(1, 2, 3, 4, 5,6);

        verify(widgetService).getAll(argument.capture());
        assertNotNull(argument.getValue());
        assertEquals(1, argument.getValue().getPage());
        assertEquals(2, argument.getValue().getSize());
        assertNotNull(argument.getValue().getFilter());
        assertEquals(3, argument.getValue().getFilter().getMinX());
        assertEquals(4, argument.getValue().getFilter().getMaxX());
        assertEquals(5, argument.getValue().getFilter().getMinY());
        assertEquals(6, argument.getValue().getFilter().getMaxY());
    }

    @Test
    public void get() throws Exception {
        controller.get("1");
        verify(widgetService).get("1");
    }

    @Test
    public void create() throws Exception {
        Widget widget = new Widget();
        controller.create(widget);
        verify(widgetService).create(widget);
    }

    @Test
    public void update() throws Exception {
        Widget widget = new Widget();
        controller.update(widget);
        verify(widgetService).update(widget);
    }

    @Test
    public void delete() throws Exception {
        controller.delete("1");
        verify(widgetService).delete("1");
    }

}