package net.yoshkin.test_job.integration;

import net.yoshkin.test_job.pojo.Widget;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.*;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

/**
 * Created by TiM on 06.10.2019.
 */
@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class WidgetControllerReadIntegrationTest {

    @LocalServerPort
    private int port;

    TestRestTemplate restTemplate = new TestRestTemplate();

    HttpHeaders headers = new HttpHeaders();

    @Test
    public void getAll() throws Exception {
        HttpEntity<String> entity = new HttpEntity<>(null, headers);

        ResponseEntity<List<Widget>> response = restTemplate.exchange(
                url("/api/widgets"),
                HttpMethod.GET,
                entity,
                new ParameterizedTypeReference<List<Widget>>(){});

        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertEquals(6, response.getBody().size());
        assertEquals("id3", response.getBody().get(0).getId());
        assertEquals("id1", response.getBody().get(1).getId());
        assertEquals("id2", response.getBody().get(2).getId());
        assertEquals("id4", response.getBody().get(3).getId());
        assertEquals("id5", response.getBody().get(4).getId());
        assertEquals("id6", response.getBody().get(5).getId());
    }

    @Test
    public void getAll_page() throws Exception {
        HttpEntity<String> entity = new HttpEntity<>(null, headers);

        ResponseEntity<List<Widget>> response = restTemplate.exchange(
                url("/api/widgets?page=2&size=4"),
                HttpMethod.GET,
                entity,
                new ParameterizedTypeReference<List<Widget>>(){});

        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertEquals(2, response.getBody().size());
        assertEquals("id5", response.getBody().get(0).getId());
        assertEquals("id6", response.getBody().get(1).getId());
    }

    @Test
    public void getAll_filter() throws Exception {
        HttpEntity<String> entity = new HttpEntity<>(null, headers);

        ResponseEntity<List<Widget>> response = restTemplate.exchange(
                url("/api/widgets?minx=7&maxx=19&miny=7&maxy=19"),
                HttpMethod.GET,
                entity,
                new ParameterizedTypeReference<List<Widget>>(){});

        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertEquals(3, response.getBody().size());
        assertEquals("id3", response.getBody().get(0).getId());
        assertEquals("id4", response.getBody().get(1).getId());
        assertEquals("id5", response.getBody().get(2).getId());
    }

    @Test
    public void get() throws Exception {
        HttpEntity<String> entity = new HttpEntity<>(null, headers);

        ResponseEntity<Widget> response = restTemplate.exchange(
                url("/api/widgets/id5"),
                HttpMethod.GET,
                entity,
                Widget.class);

        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertEquals("id5", response.getBody().getId());
        assertEquals(9, response.getBody().getX());
        assertEquals(9, response.getBody().getY());
        assertEquals(10, response.getBody().getWidth());
        assertEquals(10, response.getBody().getHeight());
        assertEquals(new Integer(5), response.getBody().getzIndex());
    }

    private String url(String uri) {
        return "http://localhost:" + port + uri;
    }
}
