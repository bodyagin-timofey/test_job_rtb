package net.yoshkin.test_job.integration;

import net.yoshkin.test_job.pojo.Widget;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.*;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

/**
 * Created by TiM on 06.10.2019.
 */
@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class WidgetControllerWriteIntegrationTest {

    @LocalServerPort
    private int port;

    TestRestTemplate restTemplate = new TestRestTemplate();

    HttpHeaders headers = new HttpHeaders();

    @Test
    public void create() throws Exception {
        Widget widget = new Widget()
                .setX(10)
                .setY(10)
                .setHeight(10)
                .setWidth(10);
        HttpEntity<Widget> entity = new HttpEntity<>(widget, headers);

        ResponseEntity<Widget> response = restTemplate.exchange(
                url("/api/widgets"),
                HttpMethod.POST,
                entity,
                Widget.class);

        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertNotNull(response.getBody().getId());
        assertNotNull(response.getBody().getLastDate());
        assertEquals(new Integer(7), response.getBody().getzIndex());
    }

    @Test
    public void update() throws Exception {
        Widget widget = new Widget()
                .setId("id3")
                .setX(100)
                .setY(100)
                .setHeight(100)
                .setWidth(100);
        HttpEntity<Widget> entity = new HttpEntity<>(widget, headers);

        ResponseEntity<Widget> response = restTemplate.exchange(
                url("/api/widgets"),
                HttpMethod.PUT,
                entity,
                Widget.class);

        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertEquals(100, response.getBody().getX());
        assertEquals(100, response.getBody().getY());
        assertEquals(100, response.getBody().getHeight());
        assertEquals(100, response.getBody().getWidth());
    }

    @Test
    public void delete() throws Exception {
        HttpEntity<String> entity = new HttpEntity<>(null, headers);

        ResponseEntity<String> response = restTemplate.exchange(
                url("/api/widgets/id4"),
                HttpMethod.DELETE,
                entity,
                String.class);

        assertEquals(HttpStatus.NO_CONTENT, response.getStatusCode());
    }

    private String url(String uri) {
        return "http://localhost:" + port + uri;
    }
}
