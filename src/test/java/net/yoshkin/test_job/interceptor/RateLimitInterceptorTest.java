package net.yoshkin.test_job.interceptor;

import org.junit.Before;
import org.junit.Test;
import org.springframework.http.HttpStatus;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.mock.web.MockHttpServletResponse;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import static org.junit.Assert.*;

/**
 * Created by TiM on 05.10.2019.
 */
public class RateLimitInterceptorTest {

    private RateLimitInterceptor interceptor;

    @Before
    public void setUp() throws Exception {
        interceptor = new RateLimitInterceptor(3);
    }

    @Test
    public void preHandle() throws Exception {
        HttpServletRequest request = new MockHttpServletRequest();
        HttpServletResponse response = new MockHttpServletResponse();

        boolean actual = interceptor.preHandle(request, response, null);
        assertEquals(true, actual);
        assertEquals("3", response.getHeader(RateLimitInterceptor.RATE_LIMIT_HEADER));
        assertEquals("2", response.getHeader(RateLimitInterceptor.REQUESTS_LEFT_HEADER));


        actual = interceptor.preHandle(request, response, null);
        assertEquals(true, actual);
        assertEquals("1", response.getHeader(RateLimitInterceptor.REQUESTS_LEFT_HEADER));

        actual = interceptor.preHandle(request, response, null);
        assertEquals(true, actual);
        assertEquals("0", response.getHeader(RateLimitInterceptor.REQUESTS_LEFT_HEADER));

        actual = interceptor.preHandle(request, response, null);
        assertEquals(false, actual);
        assertEquals(HttpStatus.TOO_MANY_REQUESTS.value(), response.getStatus());
        assertEquals("0", response.getHeader(RateLimitInterceptor.REQUESTS_LEFT_HEADER));
    }

}