package net.yoshkin.test_job.service;

import net.yoshkin.test_job.mapper.WidgetMapper;
import net.yoshkin.test_job.pojo.ListParam;
import net.yoshkin.test_job.pojo.Widget;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * Created by TiM on 06.10.2019.
 */
public class WidgetServiceDbImplTest {

    private WidgetServiceDbImpl service;
    private WidgetMapper widgetMapper;
    private ListParamUtilService listParamUtilService;

    @Before
    public void setUp() throws Exception {
        widgetMapper = mock(WidgetMapper.class);
        listParamUtilService = mock(ListParamUtilServiceImpl.class);
        service = new WidgetServiceDbImpl(widgetMapper, listParamUtilService, new WidgetCheckFieldsServiceImpl());
    }

    @Test
    public void getAll() throws Exception {
        ListParam param = new ListParam();
        service.getAll(param);

        verify(listParamUtilService).prepareListParam(param);
        verify(widgetMapper).getAll(param);
    }

    @Test
    public void get() throws Exception {
        service.get("1");

        verify(widgetMapper).get("1");
    }

    @Test
    public void create_null_zindex() throws Exception {
        Widget widget = new Widget()
                .setX(10)
                .setY(20)
                .setWidth(40)
                .setHeight(50);
        when(widgetMapper.getMaxZIndex()).thenReturn(8);
        Widget actual = service.create(widget);

        verify(widgetMapper).create(widget);

        assertNotNull(widget);
        assertNotNull(widget.getId());
        assertEquals(new Integer(9), widget.getzIndex());
    }

    @Test
    public void create_null_zindex_empty_list() throws Exception {
        Widget widget = new Widget()
                .setX(10)
                .setY(20)
                .setWidth(40)
                .setHeight(50);
        when(widgetMapper.getMaxZIndex()).thenReturn(null);
        Widget actual = service.create(widget);

        verify(widgetMapper).create(widget);

        assertNotNull(widget);
        assertNotNull(widget.getId());
        assertEquals(new Integer(1), widget.getzIndex());
    }

    @Test
    public void create_not_null_zindex() throws Exception {
        Widget widget = new Widget()
                .setX(10)
                .setY(20)
                .setzIndex(30)
                .setWidth(40)
                .setHeight(50);
        Widget actual = service.create(widget);

        verify(widgetMapper).updateAllZIndex(30);
        verify(widgetMapper).create(widget);

        assertNotNull(widget);
        assertNotNull(widget.getId());
        assertEquals(new Integer(30), widget.getzIndex());
    }

    @Test(expected = IllegalArgumentException.class)
    public void create_zero_width() throws Exception {
        Widget widget = new Widget()
                .setX(10)
                .setY(20)
                .setzIndex(30)
                .setHeight(50);
        Widget actual = service.create(widget);
    }

    @Test(expected = IllegalArgumentException.class)
    public void create_zero_height() throws Exception {
        Widget widget = new Widget()
                .setX(10)
                .setY(20)
                .setzIndex(30)
                .setWidth(40);
        Widget actual = service.create(widget);
    }

    @Test
    public void update_null_zindex() throws Exception {
        Widget widget = new Widget()
                .setId("1")
                .setX(10)
                .setY(20)
                .setWidth(40)
                .setHeight(50);
        when(widgetMapper.getMaxZIndex()).thenReturn(8);
        Widget actual = service.update(widget);

        verify(widgetMapper).update(widget);

        assertNotNull(widget);
        assertEquals("1", widget.getId());
        assertEquals(new Integer(9), widget.getzIndex());
    }

    @Test
    public void update_not_null_zindex() throws Exception {
        Widget widget = new Widget()
                .setId("1")
                .setX(10)
                .setY(20)
                .setzIndex(30)
                .setWidth(40)
                .setHeight(50);
        Widget actual = service.update(widget);

        verify(widgetMapper).updateAllZIndex(30);
        verify(widgetMapper).update(widget);

        assertNotNull(widget);
        assertEquals("1", widget.getId());
        assertEquals(new Integer(30), widget.getzIndex());
    }

    @Test(expected = IllegalArgumentException.class)
    public void update_zero_width() throws Exception {
        Widget widget = new Widget()
                .setId("1")
                .setX(10)
                .setY(20)
                .setzIndex(30)
                .setHeight(50);
        Widget actual = service.update(widget);
    }

    @Test(expected = IllegalArgumentException.class)
    public void update_zero_height() throws Exception {
        Widget widget = new Widget()
                .setId("1")
                .setX(10)
                .setY(20)
                .setzIndex(30)
                .setWidth(40);
        Widget actual = service.update(widget);
    }

    @Test
    public void delete() throws Exception {
        service.delete("1");

        verify(widgetMapper).delete("1");
    }

}