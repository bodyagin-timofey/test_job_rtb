package net.yoshkin.test_job.service;

import net.yoshkin.test_job.pojo.ListParam;
import net.yoshkin.test_job.pojo.Widget;
import net.yoshkin.test_job.pojo.WidgetFilter;
import org.junit.Before;
import org.junit.Test;

import java.time.Clock;
import java.time.Instant;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * Created by TiM on 05.10.2019.
 */
public class WidgetServiceImplTest {

    private List<Widget> widgetList;
    private WidgetService service;
    private Clock clock;
    Instant now = Instant.now();

    @Before
    public void setUp() throws Exception {
        widgetList = new CopyOnWriteArrayList<>();
        widgetList.add(new Widget().setId("1").setX(10).setY(10).setHeight(10).setWidth(10).setzIndex(1));
        widgetList.add(new Widget().setId("2").setX(20).setY(20).setHeight(20).setWidth(20).setzIndex(2));
        widgetList.add(new Widget().setId("4").setX(40).setY(40).setHeight(40).setWidth(40).setzIndex(4));

        clock = mock(Clock.class);
        when(clock.instant()).thenReturn(now);
        service = new WidgetServiceImpl(widgetList, clock,
                                        new ListParamUtilServiceImpl(10, 500),
                                        new WidgetCheckFieldsServiceImpl());
    }

    @Test
    public void getAll() throws Exception {
        List<Widget> list = service.getAll(new ListParam());

        assertEquals(3, list.size());
        assertEquals("1", list.get(0).getId());
        assertEquals("2", list.get(1).getId());
        assertEquals("4", list.get(2).getId());
    }

    @Test
    public void getAll_pagination() throws Exception {
        widgetList.clear();
        for (int i = 0; i < 1000; i++) {
            widgetList.add(new Widget().setId(String.valueOf(i)).setWidth(10).setHeight(10));
        }

        List<Widget> list = service.getAll(new ListParam().setPage(1));
        assertEquals(10, list.size());

        list = service.getAll(new ListParam().setPage(1).setSize(1000));
        assertEquals(500, list.size());

        list = service.getAll(new ListParam().setPage(1).setSize(1000));
        assertEquals(500, list.size());

        list = service.getAll(new ListParam().setPage(3).setSize(499));
        assertEquals(2, list.size());

        list = service.getAll(new ListParam().setPage(10).setSize(500));
        assertEquals(0, list.size());
    }

    @Test
    public void getAll_filter() throws Exception {
        WidgetFilter filter = new WidgetFilter();
        ListParam param = new ListParam().setFilter(filter);

        List<Widget> list = service.getAll(param);
        assertEquals(3, list.size());
        assertNull(param.getFilter());

        filter = new WidgetFilter();
        param.setFilter(filter);
        filter.setMaxX(80).setMaxY(80);
        list = service.getAll(param);
        assertEquals(3, list.size());

        filter.setMaxX(60).setMaxY(60);
        list = service.getAll(param);
        assertEquals(2, list.size());

        filter.setMinX(11).setMaxX(20).setMinY(11).setMaxY(20);
        list = service.getAll(param);
        assertEquals(0, list.size());
    }

    @Test
    public void get() throws Exception {
        Widget widget = service.get("2");

        assertEquals("2", widget.getId());

        widget = service.get("3");

        assertNull(widget);
    }

    @Test
    public void create_null_zindex() throws Exception {
        Widget widget = new Widget().setHeight(5).setWidth(5);
        widget = service.create(widget);

        assertNotNull(widget);
        assertNotNull(widget.getId());
        assertEquals(now, widget.getLastDate());
        assertEquals(4, widgetList.size());
        assertEquals(widget.getId(), widgetList.get(3).getId());
        assertEquals(new Integer(5), widgetList.get(3).getzIndex());
    }

    @Test
    public void create_negative_zindex() throws Exception {
        Widget widget = new Widget().setzIndex(-10).setHeight(5).setWidth(5);
        widget = service.create(widget);

        assertNotNull(widget);
        assertNotNull(widget.getId());
        assertEquals(now, widget.getLastDate());
        assertEquals(4, widgetList.size());
        assertEquals(widget.getId(), widgetList.get(0).getId());
        assertEquals(new Integer(-10), widgetList.get(0).getzIndex());
        assertEquals(new Integer(2), widgetList.get(1).getzIndex());
        assertEquals(new Integer(3), widgetList.get(2).getzIndex());
        assertEquals(new Integer(5), widgetList.get(3).getzIndex());
    }

    @Test
    public void create_middle_zindex() throws Exception {
        Widget widget = new Widget().setzIndex(2).setHeight(5).setWidth(5);
        widget = service.create(widget);

        assertNotNull(widget);
        assertNotNull(widget.getId());
        assertEquals(now, widget.getLastDate());
        assertEquals(4, widgetList.size());
        assertEquals(widget.getId(), widgetList.get(1).getId());
        assertEquals(new Integer(1), widgetList.get(0).getzIndex());
        assertEquals(new Integer(2), widgetList.get(1).getzIndex());
        assertEquals(new Integer(3), widgetList.get(2).getzIndex());
        assertEquals(new Integer(5), widgetList.get(3).getzIndex());
    }

    @Test
    public void create_in_empty_list() throws Exception {
        widgetList.clear();
        Widget widget = new Widget().setHeight(5).setWidth(5);
        widget = service.create(widget);

        assertNotNull(widget);
        assertNotNull(widget.getId());
        assertEquals(now, widget.getLastDate());
        assertEquals(1, widgetList.size());
        assertEquals(widget.getId(), widgetList.get(0).getId());
        assertEquals(new Integer(1), widgetList.get(0).getzIndex());
    }

    @Test(expected = IllegalArgumentException.class)
    public void create_negative_width() throws Exception {
        Widget widget = new Widget().setWidth(-4).setHeight(10);
        service.create(widget);
    }

    @Test(expected = IllegalArgumentException.class)
    public void create_negative_height() throws Exception {
        Widget widget = new Widget().setWidth(10).setHeight(-4);
        service.create(widget);
    }

    @Test
    public void update_zindex_not_change() throws Exception {
        Widget widget = copy(widgetList.get(1));
        widget.setHeight(500);
        widget = service.update(widget);

        assertEquals(3, widgetList.size());
        assertEquals(500, widget.getHeight());
        assertEquals(500, widgetList.get(1).getHeight());
        assertEquals(now, widgetList.get(1).getLastDate());
    }

    @Test
    public void update_zindex_to_negative() throws Exception {
        Widget widget = copy(widgetList.get(1));
        widget.setHeight(500);
        widget.setzIndex(-10);
        widget = service.update(widget);

        assertEquals(3, widgetList.size());
        assertEquals(500, widget.getHeight());
        assertEquals(500, widgetList.get(0).getHeight());
        assertEquals(now, widgetList.get(0).getLastDate());
        assertEquals("2", widgetList.get(0).getId());
        assertEquals(new Integer(-10), widgetList.get(0).getzIndex());
        assertEquals(new Integer(2), widgetList.get(1).getzIndex());
        assertEquals(new Integer(5), widgetList.get(2).getzIndex());

    }

    @Test
    public void update_zindex_to_null() throws Exception {
        Widget widget = copy(widgetList.get(1));
        widget.setHeight(500);
        widget.setzIndex(null);
        widget = service.update(widget);

        assertEquals(3, widgetList.size());
        assertEquals(500, widget.getHeight());
        assertEquals(500, widgetList.get(2).getHeight());
        assertEquals(now, widgetList.get(2).getLastDate());
        assertEquals("2", widgetList.get(2).getId());
        assertEquals(new Integer(1), widgetList.get(0).getzIndex());
        assertEquals(new Integer(4), widgetList.get(1).getzIndex());
        assertEquals(new Integer(5), widgetList.get(2).getzIndex());

    }

    @Test
    public void delete() throws Exception {
        service.delete("2");

        assertEquals(2, widgetList.size());
        assertEquals("1", widgetList.get(0).getId());
        assertEquals("4", widgetList.get(1).getId());
    }

    @Test
    public void delete_wrong_id() throws Exception {
        service.delete("22");

        assertEquals(3, widgetList.size());
    }

    private Widget copy(Widget widget) {
        return new Widget()
                .setId(widget.getId())
                .setX(widget.getX())
                .setY(widget.getY())
                .setWidth(widget.getWidth())
                .setHeight(widget.getHeight())
                .setzIndex(widget.getzIndex())
                .setLastDate(widget.getLastDate());
    }
}